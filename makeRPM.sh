#!/bin/sh
ver=$1
if [ -z "$ver" ] ; then
        echo "Specify version!"
        exit 1
fi
specver=`grep 'Version:' nagios-plugins-wlcg-org.atlas.spec | cut -d' ' -f2`
if [ "$specver" != $ver ] ; then
        echo "Spec file not updated!"
        exit 1
fi

user=`whoami`

SOURCEDIR=`pwd`/SOURCE
SRPMSDIR=`pwd`/SRPMS
RPMSDIR=`pwd`/RPMS
mkdir -p $SOURCEDIR

rm    -rf tmp/nagios-plugins-wlcg-org.atlas-$ver
mkdir -p  tmp/nagios-plugins-wlcg-org.atlas-$ver

cp -a CHANGES                            tmp/nagios-plugins-wlcg-org.atlas-$ver
cp -a extras                             tmp/nagios-plugins-wlcg-org.atlas-$ver
cp -a nagios-plugins-wlcg-org.atlas.spec tmp/nagios-plugins-wlcg-org.atlas-$ver
cp -a src                                tmp/nagios-plugins-wlcg-org.atlas-$ver

cd tmp
for f in `find nagios-plugins-wlcg-org.atlas-$ver -name .git`;do rm -rf $f;done

tar zcf nagios-plugins-wlcg-org.atlas-$ver.tgz nagios-plugins-wlcg-org.atlas-$ver
mv nagios-plugins-wlcg-org.atlas-$ver.tgz $SOURCEDIR/
mkdir -p /tmp/${user}/BUILD
mkdir -p /tmp/${user}/SRPMS
#mkdir -p /tmp/${user}/RPMS

cd ..
rpmbuild --define "_source_filedigest_algorithm md5"  --define "_binary_filedigest_algorithm md5" --define 'dist .el6' --buildroot /tmp/${user}/buildroot --define '_sourcedir '$SOURCEDIR --target noarch -bs nagios-plugins-wlcg-org.atlas.spec --define '_srcrpmdir '$SRPMSDIR
rpmbuild --define "_source_filedigest_algorithm md5"  --define "_binary_filedigest_algorithm md5" --define 'dist .el6' --buildroot /tmp/${user}/buildroot --define '_sourcedir '$SOURCEDIR --target noarch -bb nagios-plugins-wlcg-org.atlas.spec --define '_rpmdir '$RPMSDIR
#rpmbuild --buildroot /tmp/${user}/buildroot -bb nagios-plugins-wlcg-org.atlas.spec
# get release number from the spec file
release=`grep Release nagios-plugins-wlcg-org.atlas.spec | awk '{ print $2 }' | cut -d"%" -f 1`
#ls -l /tmp/${user}/SRPMS/noarch/
mkdir -p BUILD
rm -f BUILD/nagios-plugins-wlcg-org.atlas-${ver}-${release}.el6.src.rpm
rm -f BUILD/nagios-plugins-wlcg-org.atlas-${ver}-${release}.el6.noarch.rpm

cp $SRPMSDIR/nagios-plugins-wlcg-org.atlas-${ver}-${release}.el6.src.rpm BUILD/
RET1=$?
cp $RPMSDIR/noarch/nagios-plugins-wlcg-org.atlas-${ver}-${release}.el6.noarch.rpm BUILD/
RET2=$?

if [ $RET1 -eq 0 -a $RET2 -eq 0 ];
then
  echo 
  echo "RPM built and copied into `pwd`/BUILD"
else
  echo "Something went wrong with the RPM building..."
fi
