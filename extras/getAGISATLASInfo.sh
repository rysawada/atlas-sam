#!/bin/bash

errorFile=/tmp/getAGISATLASInfo_error.txt

echo `date` "START" > $errorFile
if [ -e /usr/libexec/grid-monitoring/probes/org.atlas/getAGISATLASInfo.py ];
then
    python /usr/libexec/grid-monitoring/probes/org.atlas/getAGISATLASInfo.py >> $errorFile 2>&1
else
    python /afs/cern.ch/user/s/stupputi/public/ADC/atlasNagios/gymn/org.atlas/extras/getAGISATLASInfo.py >> $errorFile 2>&1
fi

if [ $? -ne 0 ];
then
  echo `date` "FINISHED" >> $errorFile
  echo "Porc, fallito"
  cat $errorFile | mail -s "Cron job DDM endpoints from AGIS failed" alessandro.di.girolamo@cern.ch,ryu.sawada@cern.ch
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_srm_storage_list_tmp.json ];
then
  echo "Generated SRM file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from AGIS failed" alessandro.di.girolamo@cern.ch,ryu.sawada@cern.ch 
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_srm_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_srm_storage_list
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_root_storage_list_tmp.json ];
then
  echo "Generated XRootD file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from AGIS failed" alessandro.di.girolamo@cern.ch,ryu.sawada@cern.ch 
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_root_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_root_storage_list
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_https_storage_list_tmp.json ];
then
  echo "Generated https file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from AGIS failed" alessandro.di.girolamo@cern.ch,ryu.sawada@cern.ch 
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_https_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_https_storage_list
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_gsiftp_storage_list_tmp.json ];
then
  echo "Generated gsiftp file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from AGIS failed" alessandro.di.girolamo@cern.ch,ryu.sawada@cern.ch 
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_gsiftp_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_gsiftp_storage_list
fi

