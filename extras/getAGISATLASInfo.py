#!/usr/bin/env python

import socket
import urllib2
import argparse
try:
    import json
except ImportError:
    import simplejson as json

workdir = '/var/lib/gridprobes/atlas.Role=production/org.atlas/DDM/'
tested_tokens = [ 'atlasdatadisk', 'atlasgroupdisk', 'atlaslocalgroupdisk', 'atlasscratchdisk', 't2atlasdatadisk', 't2atlasgroupdisk', 't2atlaslocalgroupdisk', 't2atlasscratchdisk' ]
agis_ddm = "http://atlas-agis-api.cern.ch/request/ddmendpoint/query/list/?json&state=ACTIVE&preset=dict"
srm_agis_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_srm_storage_list_tmp.json','srm_provagis.json')['lxplus' in socket.gethostname()]
xrd_agis_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_root_storage_list_tmp.json','root_provagis.json')['lxplus' in socket.gethostname()]
htp_agis_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_https_storage_list_tmp.json','https_provagis.json')['lxplus' in socket.gethostname()]
gsi_agis_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/AGIS_gsiftp_storage_list_tmp.json','gsiftp_provagis.json')['lxplus' in socket.gethostname()]

class cacher(object):

    def getAGISATLASInfo(self):

        try:
            rest_agis = urllib2.urlopen(agis_ddm)
        except urllib2.HTTPError, e:
            print e
            return
        except urllib2.URLError, e:
            print 'URLError',e.reason
            return

        ddm_json = json.load(rest_agis)
        ddm_qrt = [(el['name'],el['se']+el['endpoint'],el['token'],el['rc_site']) for name,el in ddm_json.iteritems()]
        #la riga sopra e quella sotto sono passibili di migliore organizzazione?
#        ddms = filter(lambda en: en[1][-5:] == 'disk/',
#                      list(set( [ ((em[0],em[1],em[2],em[3]),(em[0].split('_')[0]+'_GROUPDISK',em[1][0:em[1].rfind('atlasgroupdisk/')+15],em[2],em[3]))['atlasgroupdisk' in em[1] ]
#                                  for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[0].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower())
#                                                                                          for ep in tested_tokens]), ddm_qrt) ] )))
#        ddms = filter(lambda en: en[1].split('/')[-2] in tested_tokens or en[1].split('/')[-3] in tested_tokens or en[2].lower() in tested_tokens,

#        ddms = filter(lambda en: en[2].lower() in tested_tokens,
#                      list(set( [ ((em[0],em[1],em[2],em[3]),(em[0].split('_')[0]+'_GROUPDISK',em[1][0:em[1].rfind('atlasgroupdisk/')+15],em[2],em[3]))['atlasgroupdisk' in em[1]]
#                                  for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[2].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower())
#                                                                                          for ep in tested_tokens]), ddm_qrt) ] )))

        #if 'atlas/atlas' in i[1]&& and&& i[2].lower() not in i[1]
        #flt_ddm=filter(lambda it: it[2].lower() in it[1], [em for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[2].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower()) for ep in tested_tokens]), ddm_qrt)])
        flt_ddm=[em for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[2].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower()) for ep in tested_tokens]), ddm_qrt)]
        ddms = filter(lambda en: en[2].lower() in tested_tokens,
                      list(set( [ ((em[0],em[1],em[2],em[3]),(em[0].split('_')[0]+'_GROUPDISK',em[1][0:em[1].rfind('atlasgroupdisk/')+15],em[2],em[3]))['atlasgroupdisk' in em[1]]
                                  for em in flt_ddm ] )))

        srm_agis_dict = {}
        xrd_agis_dict = {}
        htp_agis_dict = {}
        gsi_agis_dict = {}
        for ddm in ddms:
            perm_list = []
            #ddm_attrs = filter(lambda el: (ddm[0].split('_')[0] in el['name'] and ddm[1] in el['se']+el['endpoint']), ddm_json.values())[0]
            if ddm[0] not in ddm_json: ## FAKE endpoint => skip
                continue
            if 'CEPH' in ddm[0]:
                continue
            ddm_attrs = ddm_json[ddm[0]]
            for prot in ddm_attrs['protocols'].keys():
                cpls = [(arr[2],arr[0]) for arr in filter(lambda prm: prm[0] in ('read_wan', 'write_wan', 'delete_wan'), ddm_attrs['protocols'][prot])]
                for cpl in cpls:
                    if 'bnl' in cpl[0]:
                        path = (prot+cpls[0][0].split('rucio/')[0],prot+(cpls[0][0][0:cpls[0][0].rfind('disk/')+5]))['T0D1' not in cpls[0][0] and 'LOCAL' not in cpls[0][0] ]
                    elif 'eos' in cpl[0] or 'anl'in prot or 'gsiftp' in prot:
                        path = (prot+cpls[0][0].split('rucio')[0],prot+(cpls[0][0][0:cpls[0][0].rfind('disk/')+5]))['sgroup' in cpls[0][0] ]
                    else:
                        path = ((prot+cpls[0][0],prot+cpls[0][0]+'/')[cpls[0][0][-1] != '/'],prot+(cpls[0][0][0:cpls[0][0].rfind('disk/')+5]))['disk' not in cpls[0][0][-5:] ]
                        if 'pnf' in path[-3:]:
                            path = prot+cpls[0][0].split('perf')[0]+'atlasgroupdisk/'

                    if 'atlasuserdisk' in path:
                        continue
                    if 'DATA' in ddm[2] and ddm[2].lower().split('atlas')[1] not in path.lower() and 'BNL' not in path:
                        continue
                    if path in perm_list:
                        continue
                    perm_list.append(path)

            for p in perm_list:
                ptc = p.split(':')[0]
                if ptc == 'srm' and not srm_agis_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    srm_agis_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})
                if ptc == 'root' and not xrd_agis_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    xrd_agis_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})
                if ptc == 'davs' and not htp_agis_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    htp_agis_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})
                if ptc == 'gsiftp' and not gsi_agis_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    gsi_agis_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})

        srmfile = open(srm_agis_file,'w')
        srmfile.write(json.dumps(srm_agis_dict, indent=4, sort_keys=True))
        srmfile.close()

        xrdfile = open(xrd_agis_file,'w')
        xrdfile.write(json.dumps(xrd_agis_dict, indent=4, sort_keys=True))
        xrdfile.close()

        htpfile = open(htp_agis_file,'w')
        htpfile.write(json.dumps(htp_agis_dict, indent=4, sort_keys=True))
        htpfile.close()

        gsifile = open(gsi_agis_file,'w')
        gsifile.write(json.dumps(gsi_agis_dict, indent=4, sort_keys=True))
        gsifile.close()

        return

getter = cacher()
getter.getAGISATLASInfo()

'''
                if len(cpls) > 1:
                    while cpls:
                        aggr = reduce(lambda x,y: ((x[0],x[1]+y[1]),(x[0],x[1]))[x[0]!=y[0]],cpls)
                        path = (prot+aggr[0],prot+(aggr[0].split('atlasgroupdisk/')[0]+'atlasgroupdisk/'))['atlasgroupdisk' in aggr[0] ]
                        perm_list.append(path)
                        cpls = filter(lambda cpl: cpl[0]!=aggr[0],cpls)
                else:
                    path = (prot+cpls[0][0],prot+(cpls[0][0].split('atlasgroupdisk/')[0]+'atlasgroupdisk/'))['atlasgroupdisk' in cpls[0][0] ]
                    perm_list.append(path)

                for ep in perm_list:
                    agis_dict[ddm[1]]['protocols'].append(ep)
'''
