import logging
import itertools
import random
import urlparse

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

CE_METRICS = (
    'org.sam.CONDOR-JobSubmit-/atlas/Role=lcgadmin',
    'org.atlas.WN-FrontierSquid-/atlas/Role=lcgadmin',
    'org.atlas.WN-cvmfs-/atlas/Role=lcgadmin',
    'org.atlas.WN-swspace-/atlas/Role=lcgadmin')

WEBDAV_METRICS = (
'webdav.HTTP-DIR_HEAD-/atlas/Role=production',
'webdav.HTTP-DIR_GET-/atlas/Role=production',
'webdav.HTTP-FILE_PUT-/atlas/Role=production',
'webdav.HTTP-FILE_GET-/atlas/Role=production',
'webdav.HTTP-FILE_OPTIONS-/atlas/Role=production',
'webdav.HTTP-FILE_MOVE-/atlas/Role=production',
'webdav.HTTP-FILE_HEAD-/atlas/Role=production',
'webdav.HTTP-FILE_HEAD_ON_NON_EXISTENT-/atlas/Role=production',
'webdav.HTTP-FILE_PROPFIND-/atlas/Role=production',
'webdav.HTTP-FILE_DELETE-/atlas/Role=production',
'webdav.HTTP-FILE_DELETE_ON_NON_EXISTENT-/atlas/Role=production',
'webdav.HTTP-TLS_CIPHERS-/atlas/Role=production'
)

METHODS = ('Get', 'Put', 'Del')
TOKENS = ('', 'DATA', 'GROUP', 'LOCALGROUP', 'SCRATCH')

SRM_METRICS = ['org.atlas.DDM-srm-%s%s-/atlas/Role=production' % (m, t)
               for (m, t) in itertools.product(METHODS, TOKENS)]
SRM_METRICS.extend(['org.atlas.DDM-srm-LsDir-/atlas/Role=production'])

GSI_METRICS = ['org.atlas.DDM-gsiftp-%s%s-/atlas/Role=production' % (m, t)
               for (m, t) in itertools.product(METHODS, TOKENS)]
GSI_METRICS.extend(['org.atlas.DDM-gsiftp-LsDir-/atlas/Role=production'])

ROOT_METRICS = ['org.atlas.DDM-root-%s%s-/atlas/Role=production' % (m, t)
                for (m, t) in itertools.product(METHODS, TOKENS)]
ROOT_METRICS.extend(['org.atlas.DDM-root-LsDir-/atlas/Role=production'])

HTTPS_METRICS = ['org.atlas.DDM-https-%s%s-/atlas/Role=production' % (m, t)
                 for (m, t) in itertools.product(METHODS, TOKENS)]
HTTPS_METRICS.extend(['org.atlas.DDM-https-LsDir-/atlas/Role=production'])

FLAVOR_MAP = {'CREAM-CE': 'cream',
              'ARC-CE': 'arc',
              'HTCONDOR-CE': 'condor',
              'GLOBUS': 'gt',
              'OSG-CE': 'gt'}


def run(url):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/x_plugins_vofeed_atlas_hosts_generated.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/x_plugins_vofeed_atlas_checks_generated.cfg
    c = Checks()
    c.add_all(CE_METRICS,     tags=["CREAM-CE", "ARC-CE", "OSG-CE", "HTCONDOR-CE", "GLOBUS"])
    c.add_all(SRM_METRICS,    tags=["SRMv2", "OSG-SRMv2", "SRM"])
    c.add_all(GSI_METRICS,    tags=["GRIDFTP"])
    c.add_all(ROOT_METRICS,   tags=["XROOTD"])
    c.add_all(HTTPS_METRICS,  tags=["WEBDAV"])
    c.add_all(WEBDAV_METRICS, tags=["WEBDAV",])

    # WEBDAV setup
    for service in services:
        host = service[0]
        flavor = service[1]
        endpoint = service[2]
        if flavor not in ["WEBDAV",]:
            continue
        se_resources = feed.get_se_resources(host, flavor)
        datadisk = [ se[1] for se in se_resources if 'DATADISK' in se[0] ]
        if not datadisk:
            log.warning("Datadisk path missing for service %s (%s)" % (host, flavor))
            continue
        if len(datadisk) > 1:
            log.warning("Multiple datadisks detected for service %s (%s)" % (host, flavor))
        if not endpoint:
            c.add('webdav.HTTP-All-/atlas/Role=production', hosts=(host,), params={ 'args' : {'--uri' : "https://"+host+datadisk[0]}, '_unique_tag':'WEBDAV'})
        else:
            uri = urlparse.urljoin(endpoint.replace("davs:", "https:"), datadisk[0])
            c.add('webdav.HTTP-All-/atlas/Role=production', hosts=(host,), params={ 'args' : {'--uri' : uri}, '_unique_tag':'WEBDAV'})

    # Queue selection algorithm for job submission
    for service in services:
        host = service[0]
        flavor = service[1]
        if flavor not in ["CREAM-CE", "ARC-CE", "OSG-CE", "HTCONDOR-CE", "GLOBUS"]:
            continue
        ce_resources = feed.get_ce_resources(host, flavor)
        if service[0] == 'ce-grid.grid.uaic.ro':
            c.add('org.sam.CONDOR-JobState-/atlas/Role=lcgadmin', ('ce-grid.grid.uaic.ro',), params={'args' : {'--resource' : 'cream://ce-grid.grid.uaic.ro/nosched/pbs/atlas'}})
        elif ce_resources:
            ce_res_default = [ e for e in ce_resources if e[2] == 'true' ]
            # picks random queues if no queue marked as ETF default
            # or more than one queue marked as ETF default
            if not ce_res_default:
                # no ETF default queue
                log.warning("No ETF default queue for service %s (%s)" % (host, flavor))
                ce_res = random.choice(ce_resources)
                batch = ce_res[0] or 'nopbs'
                queue = ce_res[1]
            elif len(ce_res_default) > 1:
                # more than one ETF default queue
                log.warning("More than one ETF default queue specified for service %s (%s)" % (host, flavor))
                ce_res = random.choice(ce_res_default)
                batch = ce_res[0] or 'nopbs'
                queue = ce_res[1]
            else:
                batch = ce_res_default[0][0] or 'nopbs'
                queue = ce_res_default[0][1]
            if flavor not in FLAVOR_MAP.keys():
                log.warning("Unable to determine type for flavour %s" % flavor)
                continue
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, host, batch, queue)
            c.add('org.sam.CONDOR-JobState-/atlas/Role=lcgadmin', (host,),
                  params={'args': {'--resource': '%s' % res}})
        else:
            log.warning("No ce_resources found for host %s, BDII will used" % host)

    c.serialize()

    # Add host groups
    sites = feed.get_groups("ATLAS_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.iteritems():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()
