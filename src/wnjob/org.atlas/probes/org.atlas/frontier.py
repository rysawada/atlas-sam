import os,sys
import commands

if os.environ.has_key('FRONTIER_SERVER'):
  fs=os.environ['FRONTIER_SERVER']
else:
  print "FRONTIER_SERVER not defined"
  sys.exit(1)

try:
  fss = fs.replace('(','').split(')')
except:
  print "FRONTIER_SERVER env not in expected format: ",fs
  sys.exit(2)
# Last entry is dummy
server=[]
proxy=[]
for s in fss[:-1]:
  if s!='':
    ss=s.split('=')
    if ss[0]=='serverurl':
      server.append(ss[1]+"/Frontier")
    elif ss[0]=='proxyurl':
      proxy.append(ss[1])
    else:
      print "FRONTIER_SERVER information not as expected"
      sys.exit(3)


if len(server)==0:
  print "ERROR: SERVER URL not defined"
  sys.exit(4)
if len(proxy)==0:
  print "NO proxyURL defined in your FRONTIER_SERVER env. Testing without setting http_proxy"
  combinations=len(server)
  proxy.append('NO_ProxyURL')
else:
  combinations=len(server)*len(proxy)

global_err=0
for srv in server:
  for pxy in proxy:
    print "\n\n------------------ ------------- "
    if pxy<>"NO_ProxyURL":
      os.environ['http_proxy']=pxy
      os.environ['HTTP_PROXY']=pxy
    else:
      os.environ['http_proxy']=""
      os.environ['HTTP_PROXY']=""
    print "serverurl=",srv,"http_proxy=",pxy,"HTTP_PROXY",pxy
    cmd='python fnget.py --url='+srv+' --sql="SELECT USER, SYSDATE,9*4 FROM DUAL"'
    err,out=commands.getstatusoutput(cmd)
    print 'exit code=',err
    if err<>0:
      global_err=global_err+1
      print "ERROR\ncmd=",cmd,"\nhttp_proxy=",os.environ['http_proxy'],"\ncmd output:",out

print " ----- ------- ------- ------ ----- \n"
if global_err==0:
  print "OK: test passed\n"
  sys.exit(0) #check the exit code: 0=OK
elif 0<global_err<combinations:
  print "WARNING: ",global_err," out of ",str(combinations)," Proxy/ServerURL combinations don't work\n"
  sys.exit(10) #10 ==WARNING (at least one combination worked)
else:
  print "  -- ERROR: none of the",str(combinations),"Proxy/ServerURL combinations works \n\n"
  sys.exit(20) #20 ==ERROR
print global_err


